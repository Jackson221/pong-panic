# Module2D Example

This is an example project, primarily showing how Module2D projects are intended to be built.


# Building & running

To build a release build and run the game, you can just type

`./RUN.sh`

It depends on

* SDL2
* SDL2_TTF
* SDL_Image
* glm (for vkrender)
* Vulkan
* glslang-tools

To install all of those on Ubuntu, run

`sudo apt install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev libvulkan-dev libglm-dev glslang-tools`

Additionally, the tools needed to build are:

* gcc >= 10.0.1 (**older versions will definitely fail**; clang is not supported)
* CMake >= 3.9

**It is highly recommended to build on Ubuntu 20.10 or later**

# Windows builds

Windows builds can be made by cross-compiling from a Linux host. Building on Windows itself is not supported and Cygwin probably will not work. You will need the [MinGW-w64](http://mingw-w64.org/) version of GCC, which should be provided by your system. Module2D requires that your mingw be built with POSIX thread support.

Additionally, for Vulkan, you will need a system install of Wine.

First, you will need to install dependancies into your system. Luckily I have automated this and it can be done just by running

`./_WININSTALLLIBS.sh`

This will install header files and .a files into /usr/x86_64-w64-mingw32/.
Additionally, it will also create a `windows/` folder and install all neccessary windows DLLs into it.

Then, to create the executable you can just run

`./WINCOMPL.sh &&
make -j$(nproc)`

The exe will be placed into the `windows/` folder.  
